import Kalkulator


def start(a, b, typ):
    if typ == 1:
        wynik = Kalkulator.dodaj(a, b)
    elif typ == 2:
        wynik = Kalkulator.odejmij(a, b)
    elif typ == 3:
        wynik = Kalkulator.pomnoz(a, b)
    elif typ == 4:
        wynik = Kalkulator.podziel(a, b)
    elif typ == 5:
        wynik = Kalkulator.modulo(a, b)
    else:
        wynik = "ERROR"
    return wynik


def getInputNumber(text):
    print(text)
    value = int(input())
#     validacja
    return value


def menu():
    a = getInputNumber("Liczba A")
    b = getInputNumber("Liczba B")


    print("1.Dodaj")
    print("2.Odejmij")
    print("3.Pomnoz")
    print("4.Podziel")
    print("5.Modulo")

    dzialanie = int(input())
    wynik = start(a, b, dzialanie)
    if wynik == "ERROR":
        print("nie podales liczby")
    else:
        print(wynik)


if __name__ == '__main__':
    # Petla nieskonczona
    while True:
        menu()
        print("q to exit, or any key to repeat")
        print("Nacisnij q zeby zakonczyc")
        val = input()
        if val == "q":
            break
